
import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import App from './lib/app';

export default class Sigma extends Component {
  render() {
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('Sigma', () => Sigma);
