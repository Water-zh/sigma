import React, { Component } from 'react';
import { 
    NativeModules, 
    StatusBar, 
    View, 
    Text, 
    StyleSheet, 
    AsyncStorage 
} from 'react-native';

import { Navigator } from "react-native-deprecated-custom-components";
import Storage from 'react-native-storage';
// react-native-material-ui

import Container from './component/Container';
import OrderContent from './component/OrderContent';
import routes from './routes';
import config from '../config';
// const PushNotification = require("react-native-push-notification");
const UIManager = NativeModules.UIManager;

const styles = StyleSheet.create({
    content : {
        flex : 1,
        marginTop : 30
    }
});

class App extends Component{
    static configureScene(route) {
        return route.animationType || Navigator.SceneConfigs.FloatFromRight;
    }
    static renderScene(route, nav){
        return(
            <Container>
                <StatusBar translucent backgroundColor='black' />
                <View style={styles.content}>
                    <route.Page route={route} navigator={nav} { ...route.params } />
                </View>
            </Container>
        );
    }
    componentWillMount(){
        if( UIManager.setLayoutAnimationEnabledExperimental ){
            UIManager.setLayoutAnimationEnabledExperimental(true)
        }
        
        // PushNotification.configure({
        //     permissions : {
        //         alert : true,
        //         badge : true,
        //         sound : true
        //     }
        // });

        var storage = new Storage({
            // maximum capacity, default 1000 
            size: 1000,

            // Use AsyncStorage for RN, or window.localStorage for web.
            // If not set, data would be lost after reload.
            storageBackend: AsyncStorage,
            
            // expire time, default 1 day(1000 * 3600 * 24 milliseconds).
            // can be null, which means never expire.
            defaultExpires: 1000 * 3600 * 2, //兩小時
            
            // cache data in the memory. default is true.
            enableCache: true,
        })
        global.storage = storage;
        global.config = config;

    }
    render(){
        return (
            <Navigator
                configureScene={App.configureScene}
                initialRoute={( global.config.isProduction )? routes.login : {
                    title : 'order content component',
                    Page : OrderContent,
                } }
                renderScene={App.renderScene}
            />
        );
    }
}

export default App;

