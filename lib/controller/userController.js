const crypto = require('crypto-js');
import config from '../../config';

const API_LINK_ROUTE = `user`;
class userController{

    async postAPI(API_LINK_ROUTE, JSONbody){
        let json = JSON.stringify({ send_req : JSONbody });
        let respone = await fetch(`${config.API_LINK}${API_LINK_ROUTE}`, {
            method : 'POST',
            headers :{
                "Accept" : "application/json",
                "Content-Type" : 'application/json'
            },
            body : json
        });
        let datas = await respone.text();
        let unHash =crypto.AES.decrypt(datas.toString(), config.privateKey);

        return JSON.parse( unHash.toString( crypto.enc.Utf8 ) );
    }
    setPostprivateKey(jsonObj){
        return crypto.AES.encrypt( JSON.stringify(jsonObj), config.privateKey ).toString();
    }
}
export default userController;