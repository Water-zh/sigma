import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textRow: {
        flexDirection : "row",
        marginLeft : 10
    },
    textBox : {
        // borderWidth : 1,
        borderColor : '#fcc',
        width : 175,
        height : 40,
        backgroundColor : 'skyblue',

    },
    text : {
        marginTop : 2,
        textAlign : 'center'
    },
    list : {
        borderBottomColor : '#939a93',
        borderBottomWidth : 1,
        marginBottom : 5
    },
    content : {
    }
});