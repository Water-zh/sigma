import React, { Component, PropTypes } from 'react';
import { View, Modal } from 'react-native';
import { Text, FormLabel, FormInput, Button } from 'react-native-elements';

import Password from './Password';
import MessageText from './MessageText';
import userController from '../controller/userController';
import config from '../../config';

const user = new userController();
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Forget extends Component{
    constructor(props, context){
        super(props, context);
        this.state = {
            account : '',
            email : '',
            message : '',
            verify : '',
            showVerify : false,
            userInfo : {},
            modalVisible : false
        }

    }
    async _handelAccountEndEdit(){
        //驗證使用者帳號
        let jsonBody = user.setPostprivateKey({ account : this.state.account });
        let resq = await user.postAPI(`api/user/check-account`,jsonBody);
        let message = '';

        if( resq.userInfo ){
            message = `歡迎, ${resq.userInfo.MF002}\n 請輸入信箱收取驗證碼`;
        }else{
            message = resq.message;
        }

        this.setState({ message :  message });
    }
    async authVerify(API_ROUTE, JSONbody){
        try {
            return await user.postAPI(`api/user/${API_ROUTE}`, JSONbody);
        } catch (error) {
            console.error( error );
        }
    }
    async _handelSubmitData(){
        let message = '';
        if( this.state.account && this.state.email ){
            let API_ROUTE = `forget-password`;
            if( this.state.verify ) API_ROUTE = `auth-verify`;

            let JSONbody = user.setPostprivateKey({
                account : this.state.account,
                email : this.state.email,
                verify : this.state.verify
            });
            let checkVerify = await this.authVerify(API_ROUTE, JSONbody);

            switch (API_ROUTE) {
                case "forget-password":
                    this.setState({ showVerify : true });
                    message = '請到填寫的信箱中檢視驗證碼';
                    break;
                case "auth-verify":
                    if( checkVerify.answer ){
                        //b3031717@gmail.com
                        //zQbSCq
                        message =`${this.state.account} 驗證過了`;
                        this.setState({ 
                            userInfo : checkVerify.userInfo,
                            modalVisible : true 
                        });

                    }else{
                        message = checkVerify.message;
                    }
                    break;
            }

        }else{
            message = '請輸入帳號與信箱';
        }

        this.setState({
            message : message
        });
    }
    _verifyCodeInput(App){
        if( this.state.showVerify ){
            return (
                <View>
                    <FormLabel>驗證碼：</FormLabel>
                    <FormInput 
                        onChangeText={(verify)=> App.setState({ verify : verify }) }
                        ref={(el)=>{ this.verify = el }}
                    />
                </View>
            );
        }else{
            return <Text></Text>;
        }

    }
    render(){
        return(
            <View>
                <Text h2 style={{ textAlign : 'center' }}>忘記密碼</Text>
                <MessageText message={this.state.message} />
                <FormLabel>帳號：</FormLabel>
                <FormInput 
                    onChangeText={(account)=> this.setState({ account : account }) }
                    onEndEditing={this._handelAccountEndEdit.bind(this)}
                    ref={(el)=>{ this.account = el }}
                />
                <FormLabel>驗證信箱：</FormLabel>
                <FormInput
                    onChangeText={(email)=> this.setState({ email : email }) }
                    ref={(el)=>{ this.email = el }}
                />
                { this._verifyCodeInput(this) }
                <Text></Text>
                <Button title="送出" onPress={this._handelSubmitData.bind(this)} />
                <View>
                    <Modal
                        animationType={"slide"}
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {return true}}
                    >
                        <Password 
                            route={this.props.route} 
                            navigator={this.props.navigator} 
                            userInfo={this.state.userInfo} 
                        />
                    </Modal>
                </View>
            </View>
        );
    }
}
Forget.propTypes = propTypes
export default Forget;