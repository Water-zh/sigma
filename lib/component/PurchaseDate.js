import React, { Component, PropTypes } from 'react';
import { View, Alert } from 'react-native'
import { Text, Button } from 'react-native-elements';
import LightBox from "react-native-lightbox";
import Calendar from 'react-native-calendar-picker';

import styles from "../style";
import Controller from '../controller/userController';
import Header from './Header';
import MessageText from "./MessageText";
const dateformat = require('dateformat');
const publicClass = new Controller();

class PurchaseDate extends Component{
    constructor(props, context){
        super(props, context);
        this.state = {
            watchOrder : global.orderContent,
            stockIncomeDate : '',
            message : ''
        };
    }
    async _handleConfirmDate(){
        let { stockIncomeDate } = this.state;
        let { Purchase, PartDetail } = this.props;
        let message = '';
        
        if( stockIncomeDate ){
            let json = publicClass.setPostprivateKey({ 
                purchaseNo : Purchase.TD002, 
                partNo : PartDetail.TB003,
                incomeDate : dateformat(stockIncomeDate, "isoDate").replace(/-/gi, '')
            });
            let result = await publicClass.postAPI(`api/purchase/set-income-date`, json);
            if( result.answer ){
                Alert.alert(`交貨日期已儲存`);    
            }else{
                this.setState({ message :result.message });
            }
            
        }else{
            Alert.alert(`尚未填寫交貨日期`);
        }
    }
    render(){
        let { Purchase, PartDetail } = this.props;
        let { watchOrder } = this.state;
        let onlineDate = ( global.config.isProduction )? watchOrder.TA009 : '20170610'; //格式 ：20170501

        return(          
            <View> 
                <Header title={`採購單號：${Purchase.TD002}`} />
                <Header title={`組件名稱：${PartDetail.TB012} [ 品號: ${PartDetail.TB003} ]`} />
                <Header title={`組件規格：${PartDetail.TB013}`} />
                <Text></Text>
                <Header title={`目前觀看工單：${watchOrder.TA002}`} />
                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>預計開工</Text>
                        <Text style={styles.text}>{ ( watchOrder ) ? watchOrder.TA009 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>預計完工</Text>
                        <Text style={styles.text}>{ ( watchOrder ) ? watchOrder.TA010 : "" }</Text>
                    </View>
                </View>
                <Text></Text>
                <Header title={`交貨日期回填`} />
                <MessageText message={this.state.message} />
                <Calendar 
                    minDate={new Date()}
                    maxDate={new Date(onlineDate.substr(0, 4), parseInt(onlineDate.substr(4,2))-1, onlineDate.substr(6,2) )}
                    onDateChange={async stockIncomeDate => {
                        await this.setState({ stockIncomeDate })
                    }}
                />
                <Button title={`確認交貨時間`} onPress={this._handleConfirmDate.bind(this)}/>
            </View>
        );
    }
}
PurchaseDate.propTypes = {
    Purchase : PropTypes.object.isRequired,
    PartDetail : PropTypes.object.isRequired
};

export default PurchaseDate;