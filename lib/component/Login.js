import React, { Component, PropTypes } from 'react';
import { View } from 'react-native';
import { Text, FormLabel, FormInput, Button } from 'react-native-elements';

import routes from '../routes';
import MessageText from './MessageText';
import Header from './Header';
import userController from '../controller/userController';

const user = new userController();
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Login extends Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            userAccount : '',
            _password : '',
            message : ''
        };
        
    }
    async _handelSignIn(){
        //驗證使用者
        let message = '';
        if( !this.state.userAccount ) message = '請輸入帳號,';
        if( !this.state._password ) message += "請輸入密碼";

        let jsonBody = user.setPostprivateKey({
            account : this.state.userAccount,
            _password : this.state._password
        });
        let result = await user.postAPI(`api/user/auth-password`, jsonBody);
        if( result.answer ){
            result.userInfo.ROLE = result.userRole;
            global.storage.save({
                key : 'userInfo',
                rawData : result.userInfo
            });
            this.props.navigator.push( routes.home );
        }

    }
    _handelForget(){ 
        this.props.navigator.push( routes.forget ); 
    }

    async _handelAccountEndEdit(){
        //驗證使用者帳號
        let message = '';
        if( this.state.userAccount ){
            let jsonBody = user.setPostprivateKey({ account : this.state.userAccount });
            let result = await user.postAPI(`api/user/check-account`,jsonBody);


            if( result.answer && result.userInfo ){
                message = `歡迎, ${result.userInfo.MF002}`;
            }else if( result.userInfo ){
                message = `歡迎, ${result.userInfo.MF002}\n ${result.message}`;
            }else{
                message = result.message;
            }
        }

        this.setState({
            message : message
        });
    }
    _handelPasswordEdit(password){
        let message = '';
        if( !this.state.userAccount ) message = '請輸入帳號,';
        if( !this.state._password ) message += "請輸入密碼";
        
        this.setState({ message :  message });

    }
    render(){
        return (
            <View>
                <Header title="Sigma"/>
                <MessageText message={this.state.message}/>
                <FormLabel>帳號 ：</FormLabel>
                <FormInput 
                    onChangeText={(account)=>{ this.setState({ userAccount : account }) }} 
                    onEndEditing={this._handelAccountEndEdit.bind(this)}
                    ref={(el)=>{ this.anccount = el }}
                />
                <FormLabel>密碼 ：</FormLabel>

                <FormInput 
                    secureTextEntry={true}
                    onChangeText={(password)=> this.setState({ _password : password }) }
                    onEndEditing={this._handelPasswordEdit.bind(this)}
                    ref={(el)=>{ this.password = el }}
                />

                <Text></Text>
                <Button backgroundColor='' icon={{ name : 'envira' , type: 'font-awesome' }} title='登入' onPress={this._handelSignIn.bind(this)}/>
                <Text></Text>
                <Button backgroundColor="red" title="忘記密碼 /  首次登入" onPress={this._handelForget.bind(this)}/>
            </View>
        );
    }
}
Login.propTypes = propTypes
export default Login;
