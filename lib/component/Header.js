import React, { Component, PropTypes } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';

const propTypes = {
    title: PropTypes.string.isRequired,
};

class Header extends Component{
    render(){
        return(
            <View>
                <Text h5
                      style={{
                          textAlign : 'center',
                          // borderBottomWidth: 1,
                          // borderBottomColor : "#000"
                      }}
                >{this.props.title}</Text>
            </View>
        );
    }
}
Header.propTypes = propTypes

export default Header;