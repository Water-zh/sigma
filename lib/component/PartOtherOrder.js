import React, { Component, PropTypes } from 'react';
import { View, TouchableHighlight, StyleSheet, ScrollView, Modal, Alert } from 'react-native';
import { Text, Button, ButtonGroup } from 'react-native-elements';

import styles from "../style";
import Header from './Header';
import MessageText from './MessageText';
import Controller from '../controller/userController';
import OrderContent from "./OrderContent";
import PurchaseDate from "./PurchaseDate";

const publicClass = new Controller();

class PartOtherOrder extends Component{
    constructor(props, context){
        super(props, context );
        this.state = {
            partNumber : '',
            partMsg :'',
            partOther: {}, //同一個組件其他工單內容
            detailScrollContent : [],
            purchasContent : [],
            thisPurchaseData : {},
            BtnGroupSelected : 0,
            purchaseModelShow : false
        };
        this._loadPartOtherOrder();
    }   
    async _loadPartOtherOrder(){
        let { partDetail, partNo, orderNo } = this.props;

        let json = publicClass.setPostprivateKey({ partNo : partNo, orderNo : orderNo });
        let result = await publicClass.postAPI(`api/plant/part/orther-order`, json);
        if( result.answer ){
            let countNeed = 0;
            let message = '';
            let eleRow = [];
            let dataRow = result.data;
            let stock = ( partDetail && partDetail.detail.MC007 !== null  )? parseInt( partDetail.detail.MC007 ) : 0;

            for( let v=0; v< dataRow.length; v++){
                let thisOther = dataRow[v];
                countNeed += parseInt(thisOther.TB004);
                eleRow.push(
                    <TouchableHighlight key={v}
                        onPress={()=>{
                            this._handleOther(this, thisOther);
                        }}
                    >
                        <View style={styles.list}>
                            <Text style={styles.text}>工單 ：{thisOther.TA002}</Text>
                            <Text style={styles.text}>預計領料 ：{thisOther.TA009}</Text>
                            <Text style={styles.text}>需要量 ：{thisOther.TA015}   已領用 ：{thisOther.TA016}</Text>                        
                        </View>
                    </TouchableHighlight>
                );
            }
            message = `此組件還有${dataRow.length}筆工單需要使用，總需求：${countNeed}，`;
            
            if( stock-countNeed > 0 ){
                message +=`尚餘${stock-countNeed}`;
            }else if( stock-countNeed < 0 ){
                message +=`短缺${stock-countNeed}`;     
            }

            await this.setState({ partOther : eleRow, detailScrollContent :eleRow , partMsg : message });
        }else{
            this.setState({ partMsg : result.message });
        }
        //順便抓「採購紀錄」
        this._getPuchasRecord();
    }
    async _handlePurchaseClick(app, thisRow){
        let userRole = global.storage.load({ key : "userInfo" }).then(user=>{ return user });

        //採購與管理者權限可以設定交貨日期
        if( userRole.ME001 === "PMC" || userRole.ME005 === "Y" ){
            if( !thisRow.TD012 ){
                await this.setState({ thisPurchaseData : thisRow });
                this.setState({ purchaseModelShow : true });
            }else{
                Alert.alert(`預計交貨日期已填寫，已不能更改。`);
            }
        }else{
            Alert.alert(`只有採購與管理者帳號可以設定交貨日期`);
        }

    }
    async _getPuchasRecord(){
        let eleRow = [];
        let json = publicClass.setPostprivateKey({ partNo : this.props.partNo });
        let result = await publicClass.postAPI(`api/purchase/list`, json);
        if( result.answer ){
            let datas = result.data;
            for( let k=0; k<datas.length; k++ ){
                let thisRow = datas[k];
                let stockEnd = ( thisRow.TD016 === 'N' )? "未結束" : '已結束';
                let stateText = ( thisRow.TD025 === 'Y' )? "急料" : "非急料"
                let stateColor = ( thisRow.TD025 === 'Y' )? '#b13f3f' : "#44963c";

                eleRow.push(
                    <TouchableHighlight key={k} onPress={()=>{
                        this._handlePurchaseClick(this, thisRow);
                    }}>
                        <View style={styles.list}>
                            <Text style={styles.text}><Text style={{ color : stateColor }}>{stateText}</Text> 採購單號：{thisRow.TD002}</Text>
                            <Text style={styles.text}>採購日期：{thisRow.HEAD.TC003}  預計交貨日：{thisRow.TD012}</Text>
                            <Text style={styles.text}>採購數量：{thisRow.TD008} [{thisRow.TD009}]</Text>
                            <Text style={styles.text}>已交貨數:{thisRow.TD015} 是否交貨完畢: {stockEnd}</Text>
                        </View>
                    </TouchableHighlight>
                );
            }
        }else{
            eleRow.push(
                <Text key={0} style={styles.text}>
                    {result.message}
                </Text>
            )
        }

        this.setState({ purchasContent : eleRow });
    }
    _handleOther(app, thisOther){
        app.props.navigator.resetTo({
            title : 'show part other order',
            Page : OrderContent,
            params : {
                dataRow : thisOther.TB002
            }
        });
    }
    async _handleTabChange(BtnGroupSelected){
        await this.setState({ BtnGroupSelected });
        let { partOther, purchasContent } = this.state;

        switch (this.state.BtnGroupSelected) {
            case 0:
                this.setState({ detailScrollContent : partOther });
                break;
            case 1:
                this.setState({ detailScrollContent : purchasContent });
                break;
        }
    }
    render() {
        let { orderPartData, partDetail, orderNo } = this.props;
        return (
            <View style={{ marginTop: -18 }}>
                <View style={{ flexDirection: "row" , alignSelf : 'flex-end'}}>
                    <Button title='返回' onPress={()=>{
                        let { route, navigator } = this.props;
                        navigator.pop();

                    } } textStyle={{ color : "blue" }} buttonStyle={{ backgroundColor : 'white' }} />
                </View>
                <Header title={`所屬工單號：${orderNo} `} />
                <Header title={`組件名稱：${orderPartData.TB012} 【${orderPartData.TB007}】`} />
                <Header title={`品號：${orderPartData.TB003}`}/>
                <Header title={`規格：${orderPartData.TB013}`} />
                <Header title={`領料時間：${orderPartData.TB015}`} />

                <MessageText message={this.state.partMsg} styles={{ marginBottom : 15 }} />

                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>需要數量：</Text>
                        <Text style={styles.text}>{ ( partDetail ) ? partDetail.TB004 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>已領用量：</Text>
                        <Text style={styles.text}>{ ( partDetail ) ? partDetail.TB005 : "" }</Text>
                    </View>
                </View>
                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>庫存數：</Text>
                        <Text style={styles.text}>{ ( partDetail.detail ) ? partDetail.detail.MC007 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>安全存量：</Text>
                        <Text style={styles.text}>{ ( partDetail.detail ) ? partDetail.detail.MC004 : "" }</Text>
                    </View>
                </View>
                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>標準存量：</Text>
                        <Text style={styles.text}>{ ( partDetail.detail ) ? partDetail.detail.MC009 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                    </View>
                </View>
                <ButtonGroup
                    onPress={ this._handleTabChange.bind(this) }
                    selectedIndex={this.state.BtnGroupSelected}
                    buttons={['組件其他工單',"採購紀錄"]}
                    containerStyle={{height: 30}}
                />

                <ScrollView style={{ marginLeft : 5, marginRight : 5, marginTop : 5, height : 330}}>
                    {this.state.detailScrollContent}
                </ScrollView>
                <Modal 
                    animationType={"fade"}
                    visible={this.state.purchaseModelShow}
                    onRequestClose={ ()=> { return true }}
                >
                    <View style={{ flexDirection: "row" , alignSelf : 'flex-end', marginTop : 15, marginRight : -10}}>
                        <Button title='關閉' 
                            onPress={ async ()=>{
                                await this._getPuchasRecord();//重整採購紀錄
                                await this._handleTabChange(1);
                                this.setState({ purchaseModelShow : false });
                            }} 
                        textStyle={{ color : "blue" }} 
                        buttonStyle={{ backgroundColor : 'white' }} />
                    </View>
                    <PurchaseDate 
                        Purchase={this.state.thisPurchaseData}
                        PartDetail={this.props.orderPartData}
                    />
                </Modal>
            </View>

        );
    }
}

PartOtherOrder.propTypes = {
    partNo : PropTypes.string.isRequired,
    orderNo : PropTypes.string.isRequired,
    partDetail : PropTypes.object.isRequired,
    orderPartData : PropTypes.object.isRequired,
    navigator : PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

export default PartOtherOrder;