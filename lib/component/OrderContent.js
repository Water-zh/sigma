import React, { Component, PropTypes } from 'react';
import {
  View,
  TouchableHighlight,
  ScrollView,
  Modal,
  Image
} from 'react-native'
import { Text, ButtonGroup, Button } from 'react-native-elements';
import LightBox from "react-native-lightbox";

import styles from "../style";
import routes from "../routes";
import Controller from '../controller/userController';
import Header from './Header';
import MessageText from "./MessageText";
import PartOtherOrder from "./PartOtherOrder";

import { ScrollableTabView } from 'react-native-scrollable-tab-view';

const publicClass = new Controller();

class OrderContent extends Component{
    constructor(props, context){
        super(props, context);

        this.state = {
            orderNumber : ( global.config.isProduction )? this.props.dataRow : '170125003', // 21270031422 吸顶罩 322 HSH-033
            message : ``,
            orderData : {},
            orderPartData : {},
            orderPartDetail : {},
        };

        this._getOrderData();
        
    }

    async _getOrderData():void{
        let json = publicClass.setPostprivateKey({ orderNo : this.state.orderNumber });
        let result = await publicClass.postAPI(`api/plant/detail`, json);
        if( result.answer ){
            this.setState({
                orderData : result,
                message : result.message
            });

            //儲存目前觀看的工單內容
            global.orderContent = result.result;
        }else{
            this.setState({
                message : result.message
            });
        }

    }

    async _handlePartList(obj){

        this.setState({ orderPartData : obj });
        let json = publicClass.setPostprivateKey({ partNo : obj.TB003 });
        let result = await publicClass.postAPI(`api/plant/part/detail`, json);

        if( result.answer ){

            this.props.navigator.push({
                title : "part other order",
                Page : PartOtherOrder,
                params : {
                    partNo : obj.TB003,
                    orderNo : obj.TB002,
                    partDetail : result,
                    orderPartData : obj
                }
            });
        }else{
            this.setState({
                message : result.message
            });
        }
    }
    render(){
        let texts = [];
        let orderData = this.state.orderData;
        let part = this.state.orderPartData;
        let itemName = ( orderData.result ) ? orderData.result.TA034 : '';

        //組建列表
        if( orderData.detail ){
            for( let v=0; v < orderData.detail.length; v++ ){
                let thisDetail = orderData.detail[v];
                texts.push(
                    <TouchableHighlight key={v}
                        onPress={()=>{
                            this._handlePartList(thisDetail);
                        }}
                    >
                        <View style={styles.list}>
                            <Text style={styles.text}>{thisDetail.TB012}  [ 料號: {thisDetail.TB003} ]</Text>
                            <Text style={styles.text}>規格：{thisDetail.TB013}</Text>
                            <Text style={styles.text}>預計領料 ：{thisDetail.TB015} </Text>
                        </View>
                    </TouchableHighlight>
                );
            }
        }
        return(
            <View>
                <View style={{ flexDirection: "row" , alignSelf : 'flex-end'}}>
                    <Button title='返回' textStyle={{ color : "blue" }} buttonStyle={{ backgroundColor : 'white' }} onPress={()=>{
                        let { route, navigator } = this.props;
                        //這樣篩選是因為如果剛從Home近來用resetTo會有error, 所以沒有從partOtherorder回來就用pop
                        switch (route.title) {
                            case "order content component":
                                navigator.pop();
                                break;
                            case "show part other order":
                                navigator.resetTo(routes.home);
                                break;
                        }
                    }}  />
                </View>
                <Header title={`工單：${this.state.orderNumber}`} />
                <Header title={`產品名：${itemName}`} />
                <MessageText message={this.state.message} styles={ { marginBottom : 15 }} />

                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>預計開工</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA009 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>預計完工</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA010 : "" }</Text>
                    </View>
                </View>
                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>預計產量</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA015 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>已領套數</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA016 : "" }</Text>
                    </View>
                </View>
                <View style={styles.textRow}>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>已生產套數</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA017 : "" }</Text>
                    </View>
                    <View style={styles.textBox}>
                        <Text style={styles.text}>報廢數量</Text>
                        <Text style={styles.text}>{ ( orderData.result ) ? orderData.result.TA018 : "" }</Text>
                    </View>
                </View>
                <Text></Text>

                <Header title='組件列表' />
                <View >
                    <ScrollView style={{ marginLeft : 5, marginRight : 5, marginTop : 5, height : 350}}>
                        {texts}
                    </ScrollView>
                </View>

            </View>
        );
    }
}

OrderContent.propTypes = {
    dataRow : PropTypes.string.isRequired,
    navigator : PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
export default OrderContent;

