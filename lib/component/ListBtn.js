import React, { Component, PropTypes } from 'react';
import { View, TouchableHighlight } from 'react-native';
import { Text } from 'react-native-elements';
import OrderContent from './OrderContent';

const propTypes = {
    dataRow: PropTypes.object.isRequired,
    navigator : PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class ListBtn extends Component{
    _handleBtnClick(){
        this.props.navigator.push({
            title : 'order content component',
            Page : OrderContent,
            params : {
                dataRow : this.props.dataRow.TA002
            }

        });
    }
    render(){
        return(
            <TouchableHighlight onPress={this._handleBtnClick.bind(this)}>
                <View style={{
                    backgroundColor : '#E8ECEC',
                    borderBottomColor : "#F8FAFA",
                    borderBottomWidth : 2,
                    height : 60
                }}>
                    <Text>PO Number : {this.props.dataRow.TA033}</Text>
                    <Text>{this.props.dataRow.TA034}[ 工單：{this.props.dataRow.TA002} ]</Text>
                    <Text>預計開工：{this.props.dataRow.TA009} 預計完工：{this.props.dataRow.TA010}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}

ListBtn.propTypes = propTypes
export default ListBtn;