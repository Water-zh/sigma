import React, { Component, PropTypes } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

const propTypes = {
    message : PropTypes.string.isRequired,
};

class MessageText extends Component{
    render(){
        return(
            <View>
                <Text style={{ textAlign : 'center', color : 'red', marginTop: 10 }}>{this.props.message}</Text>
            </View>
        );
    }
}

MessageText.propTypes = propTypes
export default MessageText;